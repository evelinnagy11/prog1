#include <stdio.h>

int main(){
	int a, b;

	printf("Enter the 'A' number: ");
	scanf("%d", &a);
	printf("Enter the 'B' number: ");
	scanf("%d", &b);

	printf("The 'A' is: %d\n", a);
	printf("The 'B' is: %d\n", b);

	a = b - a;
	b = b - a;
	a = b + a;

	printf("------ \nTHE MAGIC HAPPENED \n------ \n");

	printf("Now the 'A' is %d and 'B' is %d.\n", a, b);

	a = b - a;
	b = b - a;
	a = b + a;

	printf("------ \nAND IT HAPPENED AGAIN \n------\n");

	printf("And now the 'A' is %d and 'B' is %d again! \n", a, b);

	return(0);
}
